# Program Name:         Lab2.py
# Course:               IT1113/Section W01
# Student Name:         Kathryn Browning
# Assignment Number:    Lab2
# Due Date:             02/11/2018

# This program prompts for a user's name and how many books they've purchased this 
# month, and displays how many points they've earned from buying those books
def main():
    while True:
        name = input('\nPlease enter your first and last names.\n')
        books_purchased = int(input('\nPlease enter the number of books you have purchased this month.\n'))

        if books_purchased == 0:
            points = 0
        elif books_purchased == 1 or books_purchased == 2:
            points = 5
        elif books_purchased == 3 or books_purchased == 4:
            points = 10
        elif books_purchased == 5 or books_purchased == 6:
            points = 15
        elif books_purchased == 7 or books_purchased == 8:
            points = 20
        else:
            points = 25

        print('Your points are displayed below:')
        print('{:<30} {:<3}'.format(name, points))
        
        continue_program = input('Calculate more points? (y/n):')
        if continue_program != 'y':
            break

# Call main function
main()
